/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

/**
 *
 * @author Jamil
 */
public class Validaciones {

    private Validaciones() {
    }
    
    public static String validarObligatorio(String string, String mensajeError) {
        if (string == null) {
            return mensajeError;
        }
        if (string.isEmpty()) {
            return mensajeError;
        }
        return "";
    }
    
    public static String esEntero(String string, String mensajeError) {
        if (string == null) {
            return mensajeError;
        }
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            return mensajeError;
        }
        return "";
    }
    
    public static Boolean esEntero(String string) {
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;        
    }
    
    public static String enteroDentroDeRango(Integer entero, Integer numeroMenor, Integer numeroMayor, String mensajeError) {
        if(entero < numeroMenor || entero > numeroMayor) {
            return mensajeError;            
        }
        return "";
    }
    
    public static String stringDentroDeRango(String string, Integer maxCaracteres, String mensajeError) {
        if (string.length() > maxCaracteres) {
            return mensajeError;
        }
        return "";
    }
}
