/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.upc.mantenimientoproducto.util.HttpServletGeneral;

/**
 *
 * @author Jamil
 */
@WebServlet(name = "EliminarCategoria", urlPatterns = {"/EliminarCategoria"})
public class EliminarCategoria extends HttpServletGeneral {

    private static final Logger LOGGER = Logger.getLogger(EliminarCategoria.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

        String mensaje = "";
        CategoriaDao dao = new CategoriaDaoImpl();
        Integer id = Integer.parseInt(request.getParameter("id"));

        try {
            dao.eliminar(Categoria.class, id);
            mensaje = "Se eliminó la categoría satisfactoriamente.";
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Ocurrió un error", ex);
            mensaje = "Ocurrió un error. Es posible que la categoría está siendo usada por otros productos.";
        }

        response.sendRedirect("categoria/listar.jsp?mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
    }
}
