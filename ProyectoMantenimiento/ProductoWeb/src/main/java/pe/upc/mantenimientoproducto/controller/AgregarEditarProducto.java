/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.edu.upc.mantenimientoproducto.dao.Dao;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Producto;
import pe.edu.upc.mantenimientoproducto.util.FechaUtil;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.upc.mantenimientoproducto.util.HttpServletGeneral;

/**
 *
 * @author Jamil
 */
@WebServlet(name = "AgregarEditarProducto", urlPatterns = {"/AgregarEditarProducto"})
public class AgregarEditarProducto extends HttpServletGeneral {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String validar(String id, String nombre, String stock, String idCategoria, String fecha) {

        CategoriaDao categoriaDao = new CategoriaDaoImpl();

        StringBuilder mensaje = new StringBuilder();

        if (!id.isEmpty()) {
            mensaje.append(Validaciones.esEntero(id, "El ID no es valido.\n"));
        }

        if (Validaciones.esEntero(id)) {
            mensaje.append(Validaciones.enteroDentroDeRango(Integer.parseInt(id), 0, 10000, "El ID esta fuera de rango.\n"));
        }

        mensaje.append(Validaciones.validarObligatorio(nombre, "El nombre es un campo obligatorio.\n"));
        mensaje.append(Validaciones.stringDentroDeRango(nombre, 30, "El nombre es más largo que lo permitido.\n"));
        mensaje.append(Validaciones.validarObligatorio(stock, "El stock es un campo obligatorio.\n"));

        if (Validaciones.esEntero(stock)) {
            mensaje.append(Validaciones.enteroDentroDeRango(Integer.parseInt(stock), 1, 10000, "El stock debe ser valido.\n"));
        }

        mensaje.append(Validaciones.validarObligatorio(idCategoria, "La categoria es un campo obligatorio.\n"));
        mensaje.append(Validaciones.esEntero(idCategoria, "La categoria debe ser valida.\n"));
        
        if (idCategoria != null && categoriaDao.obtener(Integer.parseInt(idCategoria)) == null) {
            mensaje.append("La categoría debe estar registrada en el sistema.\n");
        }

        mensaje.append(Validaciones.validarObligatorio(fecha, "La fecha de vencimiento es obligatoria.\n"));

        if (!FechaUtil.validarFecha(
                "dd/MM/yyyy", fecha)) {
            mensaje.append("La fecha debe estar en formato dd/mm/yyyy.\n");
        }

        return mensaje.toString();
    }

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.setCharacterEncoding("UTF-8");

        Dao productoDao = new ProductoDaoImpl();
        Producto producto = new Producto();

        String mensaje;

        String id = request.getParameter("txtIdProducto").trim();
        String nombre = request.getParameter("txtNombreProducto").trim();
        String stock = request.getParameter("txtStock").trim();
        String idCategoria = request.getParameter("txtCategoria");
        String fecha = request.getParameter("txtFecha").trim();
        
        String idGenerado = null;

        mensaje = validar(id, nombre, stock, idCategoria, fecha);

        if (mensaje.isEmpty()) {

            if (!request.getParameter("txtIdProducto").isEmpty()) {
                producto.setIdProducto(Integer.parseInt(id));
                producto.setNombre(nombre);
                producto.setStock(Integer.parseInt(stock));
                Categoria categoria = new Categoria();
                categoria.setIdCategoria(Integer.parseInt(idCategoria));
                producto.setCategoria(categoria);
                producto.setFechaVencimiento(FechaUtil.stringAFecha(fecha, "dd/MM/yyyy"));
                productoDao.actualizar(producto);
                mensaje = "El producto se modificó correctamente.";

            } else {
                producto.setNombre(nombre);
                producto.setStock(Integer.parseInt(stock));
                Categoria categoria = new Categoria();
                categoria.setIdCategoria(Integer.parseInt(idCategoria));
                producto.setCategoria(categoria);
                producto.setFechaVencimiento(FechaUtil.stringAFecha(fecha, "dd/MM/yyyy"));
                productoDao.insertar(producto);
                mensaje = "El producto ha sido insertado correctamente.";
                idGenerado = producto.getIdProducto().toString();
            }
        }

        if (id.isEmpty()) {
            if (idGenerado != null) {
                response.sendRedirect("producto/listar.jsp?"
                        + "id=" + URLEncoder.encode(idGenerado, "UTF-8")
                        + "&mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
            } else {
                 response.sendRedirect("producto/listar.jsp?mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
            }           
        } else {
            response.sendRedirect("producto/listar.jsp?mensaje="
                    + URLEncoder.encode(mensaje, "UTF-8")
                    + "&id="
                    + URLEncoder.encode(id, "UTF-8"));
        }
    }
}
