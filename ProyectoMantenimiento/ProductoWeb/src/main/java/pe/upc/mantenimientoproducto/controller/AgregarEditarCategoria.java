/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.upc.mantenimientoproducto.util.HttpServletGeneral;

/**
 *
 * @author Jamil
 */
@WebServlet(name = "AgregarEditarCategoria", urlPatterns = {"/AgregarEditarCategoria"})
public class AgregarEditarCategoria extends HttpServletGeneral {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String validar(String id, String nombre, String descripcion) {

        StringBuilder mensaje = new StringBuilder();

        if (!id.isEmpty()) {
            mensaje.append(Validaciones.esEntero(id, "El ID no es valido.\n"));
        }

        if (Validaciones.esEntero(id)) {
            mensaje.append(Validaciones.enteroDentroDeRango(Integer.parseInt(id), 0, 10000, "El ID esta fuera de rango"));
        }

        mensaje.append(Validaciones.validarObligatorio(nombre, "El nombre es un campo obligatorio.\n"));
        mensaje.append(Validaciones.stringDentroDeRango(nombre, 30, "El nombre es más largo que lo permitido.\n"));
        mensaje.append(Validaciones.stringDentroDeRango(descripcion, 200, "La descripcion es mas larga que lo permitido."));

        return mensaje.toString();
    }

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        request.setCharacterEncoding("UTF-8");

        CategoriaDao categoriaDao = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        String mensaje;

        String id = request.getParameter("txtId");
        String nombre = request.getParameter("txtNombre").trim();
        String descripcion = request.getParameter("txtDescripcion").trim();

        String idGenerado = null;

        mensaje = validar(id, nombre, descripcion);

        if (mensaje.isEmpty()) {

            Categoria temporal = new Categoria();

            temporal.setNombre(nombre);

            //Actualizar
            if (!request.getParameter("txtId").isEmpty()) {

                categoria.setIdCategoria(Integer.parseInt(id));
                categoria.setNombre(nombre);
                categoria.setDescripcion(descripcion);

                if (categoriaDao.buscar(categoria) != null) {
                    mensaje = "La categoría " + nombre + " ya existe.";
                } else {
                    categoriaDao.actualizar(categoria);
                    mensaje = "La categoría se modificó satisfactoriamente.";
                }

                //Insertar
            } else {
                categoria.setNombre(nombre);
                categoria.setDescripcion(descripcion);

                if (categoriaDao.buscar(categoria) != null) {
                    mensaje = "La categoría " + nombre + " ya existe.";
                } else {
                    categoriaDao.insertar(categoria);
                    mensaje = "La categoría ha sido insertada correctamente.";
                    idGenerado = categoria.getIdCategoria().toString();
                }
            }
        }

        if (id.isEmpty()) {
            if (idGenerado != null) {
                response.sendRedirect("categoria/listar.jsp?"
                        + "id=" + URLEncoder.encode(idGenerado, "UTF-8")
                        + "&mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
            } else {
                response.sendRedirect("categoria/listar.jsp?"
                        + "mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
            }
        } else {
            response.sendRedirect("categoria/listar.jsp?mensaje="
                    + URLEncoder.encode(mensaje, "UTF-8")
                    + "&id="
                    + URLEncoder.encode(id, "UTF-8"));
        }
    }
}
