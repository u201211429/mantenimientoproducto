/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.UsuarioDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Usuario;
import pe.edu.upc.mantenimientoproducto.dao.base.UsuarioDao;
import pe.upc.mantenimientoproducto.util.HttpServletGeneral;

/**
 *
 * @author Jamil
 */
@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServletGeneral {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private String validar(String nombreUsuario, String clave) {
        
        StringBuilder mensaje = new StringBuilder();
        
        mensaje.append(Validaciones.validarObligatorio(nombreUsuario, "Se debe ingresar el nombre de usuario.\n"));    
        mensaje.append(Validaciones.validarObligatorio(clave, "Se debe ingresar la clave.\n"));
        mensaje.append(Validaciones.stringDentroDeRango(nombreUsuario,30,"El nombre es más largo que lo permitido.\n"));
        
        if (clave.length() > 50 || clave.length() < 8) {
            mensaje.append("La clave debe tener más de 8 caracteres y menos que 50.\n");
        }
        return mensaje.toString();
    }
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String mensaje;
        
        String nombreUsuario = request.getParameter("txtUsuario").trim();
        String clave = request.getParameter("txtClave").trim();
        
        mensaje = validar(nombreUsuario, clave);
        
        if (mensaje.isEmpty()) {
            Usuario usuario = new Usuario();
            usuario.setNombreUsuario(nombreUsuario);
            usuario.setClave(clave);
            
            UsuarioDao dao = new UsuarioDaoImpl();
            Usuario respuesta = dao.buscar(usuario);
            if (respuesta != null) {
                response.sendRedirect("menu.jsp?mensaje=" + "Datos correctos");
            } else {
                mensaje = "Datos incorrectos";
                response.sendRedirect("index.jsp?mensaje=" + mensaje);
            }
        } else {
            response.sendRedirect("index.jsp?mensaje=" + URLEncoder.encode(mensaje,"UTF-8"));
        }        
    }
}
