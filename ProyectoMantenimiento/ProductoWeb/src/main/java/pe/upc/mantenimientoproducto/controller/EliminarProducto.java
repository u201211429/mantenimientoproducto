/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Producto;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;
import pe.upc.mantenimientoproducto.util.HttpServletGeneral;

/**
 *
 * @author Jamil
 */
@WebServlet(name = "EliminarProducto", urlPatterns = {"/EliminarProducto"})
public class EliminarProducto extends HttpServletGeneral {
    
    private static final Logger LOGGER = Logger.getLogger(EliminarProducto.class.getName());

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String mensaje = "";
        ProductoDao dao = new ProductoDaoImpl();
        Integer id = Integer.parseInt(request.getParameter("id"));

        try {
            dao.eliminar(Producto.class, id);
            mensaje = "Se eliminó el producto satisfactoriamente.";
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Ocurrió un error", ex);
            mensaje = "Ocurrió un error al momento de eliminar.";
        }

        response.sendRedirect("producto/listar.jsp?mensaje=" + URLEncoder.encode(mensaje, "UTF-8"));
    }
}
