<%-- 
    Document   : menu
    Created on : Apr 24, 2015, 12:56:11 PM
    Author     : Jamil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% String mensaje = "";

if (request.getParameter("mensaje") != null) {
    mensaje = request.getParameter("mensaje");
}
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Menu</title>
    </head>
    <body>
        <h1>Menu</h1>
        <p id="txtMensaje"><%= mensaje %></p>
        <a href="producto/listar.jsp" id="btnProductos">Productos</a>
        <a href="categoria/listar.jsp" id="btnCategorias">Categorias</a>
    </body>
</html>
