<%-- 
    Document   : footer
    Created on : Apr 29, 2015, 6:52:36 PM
    Author     : Jamil
--%>

<!-- jQuery -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="../dist/js/sb-admin-2.js"></script>

<!-- Datepicker JavaScript-->    
<script src="../bower_components/boostrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>