<%-- 
    Document   : detalle
    Created on : Apr 22, 2015, 11:33:07 PM
    Author     : Jamil
--%>

<%@page import="pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Categoria"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.Dao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Integer id = 0;
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
    }
    CategoriaDao dao = new CategoriaDaoImpl();
    Categoria categoria = null;
    try {
        categoria = (Categoria) dao.obtener(id);
    } catch (Exception ex) {
        response.sendRedirect("listar.jsp");
    }
%>

<html>
    <head>
        <%@include file="../template/headerInterno.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detalle Categoria</title>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1 id="txtTitulo">Detalle de categoría</h1>                    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a class="btn btn-primary" href="listar.jsp">Regresar</a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="txtId">ID</label>
                                        <input class="form-control" id="txtId" type="text" name="txtId" readonly="" value=<%= (categoria == null) ? "" : categoria.getIdCategoria()%>>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtNombre">Nombre</label>
                                        <input class="form-control" id="txtNombre" type="text" name="txtNombre" readonly="" value=<%= (categoria == null) ? "" : categoria.getNombre()%>>
                                    </div>
                                    <div class="form-group">
                                        <label for="txtDescripcion">Descrpción</label>
                                        <textarea class="form-control" id="txtDescripcion" name="txtDescripcion" readonly="" ><%= (categoria == null) ? "" : categoria.getDescripcion()%></textarea>                                        
                                    </div>
                                    <%--cambiar a link--%>
                                    <a class="btn btn-primary"  href="listar.jsp">Regresar</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%@include file="../template/footerInterno.jsp" %>
    </body>
</html>
