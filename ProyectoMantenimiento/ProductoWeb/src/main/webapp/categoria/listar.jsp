<%-- 
    Document   : listar
    Created on : Apr 22, 2015, 11:12:11 PM
    Author     : Jamil
--%>

<%@page import="java.net.URLDecoder"%>
<meta charset="UTF-8">
<meta http-equiv="Content-type" content="text/html; charset=UTF-8">

<%@page import="pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.Dao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Categoria"%>
<%@page import="java.util.List"%>

<% CategoriaDao dao = new CategoriaDaoImpl();

    request.setCharacterEncoding("UTF-8");

    String mensaje = "";
    Integer id;
    Categoria categoria = new Categoria();

    List<Categoria> categorias = dao.listar();

    if (request.getParameter("mensaje") != null) {
        mensaje = request.getParameter("mensaje");
    }
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
        try {
            categoria = (Categoria) dao.obtener(id);
        } catch (Exception e) {
        }
    }
    Integer maxSize = 20;
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../template/headerInterno.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Categorias</title>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header" id="txtTitulo">Ingresar o editar categorías</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="../menu.jsp">Regresar</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <% if (!mensaje.isEmpty()) {%>
                    <div class="alert alert-warning" id="txtMensaje">
                        <%= mensaje%>
                    </div>
                    <%}%>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form action="../AgregarEditarCategoria" id="frmCategoria" method="post">
                                        <div class="form-group">
                                            <label for="txtId">ID</label>
                                            <input class="form-control" id="txtId" type="text" name="txtId" value="<%= (categoria.getIdCategoria() == null) ? "" : categoria.getIdCategoria()%>" readonly=""/>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtNombre">Nombre</label>
                                            <input class="form-control" id="txtNombre" type="text" name="txtNombre" value="<%= (categoria.getNombre() == null) ? "" : categoria.getNombre()%>" />                                        
                                        </div>
                                        <div class="form-group">
                                            <label for="txtDescripcion">Descripción</label>
                                            <textarea class="form-control" id="txtDescripcion" name="txtDescripcion" cols="40" rows="6"><%= (categoria.getDescripcion() == null) ? "" : categoria.getDescripcion()%></textarea>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Aceptar" name="btnAceptar" />
                                    </form>
                                </div>
                                <div class="col-lg-8">
                                    <table id="tabla" class="table table-striped table-bordered table-hover" border="1">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Descripción</th>
                                                <th>Detalle</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                for (Categoria entrada : categorias) {
                                            %>
                                            <tr>
                                                <td><%= entrada.getIdCategoria()%> </td>
                                                <td><%= entrada.getNombre()%></td>

                                                <% if (entrada.getDescripcion().length() < maxSize) {
                                                %>

                                                <td> <%= entrada.getDescripcion()%> </td>

                                                <%
                                                } else {
                                                %>

                                                <td> <%= entrada.getDescripcion().substring(0, maxSize) + "..."%></td>

                                                <%
                                                    }
                                                %>

                                                <% String eliminarUrl = "../EliminarCategoria?id=" + entrada.getIdCategoria();
                                                    String editarUrl = "listar.jsp?id=" + entrada.getIdCategoria();
                                                    String detalleUrl = "detalle.jsp?id=" + entrada.getIdCategoria();%>

                                                <td><a href="<%=detalleUrl%>" id="btnDetalle<%=entrada.getIdCategoria()%>">Detalle</a></td>
                                                <td><a href="<%= editarUrl%>" id="btnEditar<%= entrada.getIdCategoria()%>">Editar</a> 
                                                    <a href="<%= eliminarUrl%>" id="btnEliminar<%= entrada.getIdCategoria()%>">Eliminar</a> </td>
                                            </tr>

                                            <%
                                                }
                                            %>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../template/footerInterno.jsp" %>
        <%@include file="../template/datatablesFooter.jsp" %>
        <script src="../js/tablas.js"></script>

        <!--<script src="../js/alertas.js"></script> -->

    </body>
</html>
