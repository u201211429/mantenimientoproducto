<%-- 
    Document   : index
    Created on : Apr 22, 2015, 10:30:31 PM
    Author     : Jamil
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    request.setCharacterEncoding("UTF-8");
    
    String mensaje = "";
    if (request.getParameter("mensaje") != null) {
        mensaje = request.getParameter("mensaje");
    }

%>


<!DOCTYPE html>
<html>
    <head>

        <%@include file="template/header.jsp"%>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <% if (!mensaje.isEmpty()) {%>
                    <div class="alert alert-warning" id="txtMensaje">
                        <%= mensaje%>
                    </div>
                    <%}%>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ingreso al sistema</h3>
                        </div>
                        <div class="panel-body">
                            <form action="LoginController" id="frmLogin" method="post">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" id="txtUsuario" type="text" name="txtUsuario" placeholder="Usuario" required="" autofocus/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" id="txtClave" type="password" name="txtClave" placeholder="Clave" required=""/>
                                    </div>
                                    <div>
                                        <input type="submit" id="btnIngresar" value="Ingresar" name="btnIngresar" class="btn btn-lg btn-success btn-block" />
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>   
    <%@include file="template/footer.jsp" %>
</body>
</html>
