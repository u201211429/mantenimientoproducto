<%-- 
    Document   : listar
    Created on : Apr 22, 2015, 11:12:11 PM
    Author     : Jamil
--%>

<%@page import="pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.util.FechaUtil"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Producto"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.Dao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Categoria"%>
<%@page import="java.util.List"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl"%>

<% CategoriaDao daoCategoria = new CategoriaDaoImpl();
    List<Categoria> categorias = daoCategoria.listar();

    request.setCharacterEncoding("UTF-8");

    ProductoDao dao = new ProductoDaoImpl();

    String mensaje = "";
    Integer id;
    Producto producto = null;

    List<Producto> productos = dao.listar();
    
    if (request.getParameter("mensaje") != null) {
        mensaje = request.getParameter("mensaje");
    }
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
        try {
            producto = new Producto();
            producto = (Producto) dao.obtener(id);
        } catch (Exception e) {
        }
    }
    Integer maxSize = 20;
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="../template/headerInterno.jsp" %>
        <%@include file="../template/datatablesHeader.jsp" %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Categorias</title>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 id="txtTitulo">Ingresar o editar productos</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="../menu.jsp">Regresar</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <% if (!mensaje.isEmpty()) {%>
                    <div class="alert alert-warning" id="txtMensaje">
                        <%= mensaje%>
                    </div>
                    <%}%>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form action="../AgregarEditarProducto" id="frmProducto" method="post">
                                        <div class="form-group">
                                            <label for="txtIdProducto">ID</label>
                                            <input class="form-control" id="txtIdProducto" type="text"  name="txtIdProducto" readonly="" value="<%= producto == null ? "" : producto.getIdProducto()%>">
                                        </div>
                                        <div class="form-group">
                                            <label for="txtNombreProducto">Nombre</label>
                                            <input class="form-control" id="txtNombreProducto" type="text" maxlength="30" name="txtNombreProducto" value="<%= (producto == null) ? "" : producto.getNombre()%>">
                                        </div>
                                        <div class="form-group">
                                            <label for="txtStock">Stock</label>
                                            <input class="form-control" id="txtStock" type="text" maxlength="5" name="txtStock" value="<%= producto == null ? "" : producto.getStock()%>">
                                        </div>
                                        <div class="form-group">
                                            <label for="txtCategoria">Categoria</label>
                                            <select class="form-control" id="txtCategoria" name="txtCategoria">
                                                <% for (Categoria categoria : categorias) {%>
                                                <option value="<%=categoria.getIdCategoria()%>"><%=categoria.getNombre()%></option>
                                                <%}%>                        
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="txtFecha">Fecha</label>
                                            <input id="fecha" class="form-control" id="txtFecha" type="text" maxlength="10" name="txtFecha" value="<%= producto == null ? "" : FechaUtil.formatoDeFecha("yyyy-MM-dd", "dd/MM/yyyy", producto.getFechaVencimiento().toString())%>">
                                        </div>
                                        <input class="btn btn-primary" type="submit" value="Aceptar" name="btnAceptar" />
                                    </form> 
                                </div>
                                <div class="col-lg-8">
                                    <table id="tabla" class="table table-striped table-bordered table-hover" border="1">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Stock</th>
                                                <th>Categoria</th>
                                                <th>Fecha</th>
                                                <th>Detalle</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                for (Producto entrada : productos) {
                                            %>
                                            <tr>
                                                <td><%= entrada.getIdProducto()%> </td>
                                                <td><%= entrada.getNombre()%></td>
                                                <td> <%= entrada.getStock()%> </td>
                                                <td> <%= entrada.getCategoria().getNombre()%> </td>
                                                <td> <%= FechaUtil.formatoDeFecha("yyyy-MM-dd", "dd/MM/yyyy", entrada.getFechaVencimiento().toString())%> </td>

                                                <% String eliminarUrl = "../EliminarProducto?id=" + entrada.getIdProducto();
                                                    String editarUrl = "listar.jsp?id=" + entrada.getIdProducto();%>
                                                
                                                <td><a href="<%= "detalle.jsp?id=" + entrada.getIdProducto()%>" id="btnDetalle<%= entrada.getIdProducto() %>">Detalle</a></td>
                                                
                                                <td><a href="<%= editarUrl%>" id="btnEditar<%=entrada.getIdProducto()%>">Editar</a> | <a href="<%= eliminarUrl%>" id="btnEliminar<%=entrada.getIdProducto()%>">Eliminar</a> </td>
                                            </tr>

                                            <%
                                                }
                                            %>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <%@include file="../template/footerInterno.jsp" %>
        <%@include file="../template/datatablesFooter.jsp" %>
        
        <!-- JS para tablas -->
        <script src="../js/tablas.js"></script>
        
        <!-- JS datepicker en español -->
        <script src="../bower_components/boostrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js"></script>
        <script src="../js/datepicker.js"></script>
    </body>
</html>
