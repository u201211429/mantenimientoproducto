<%-- 
    Document   : detalle
    Created on : Apr 22, 2015, 11:33:07 PM
    Author     : Jamil
--%>

<%@page import="pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl"%>
<%@page import="pe.edu.upc.mantenimientoproducto.util.FechaUtil"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Producto"%>
<%@page import="pe.edu.upc.mantenimientoproducto.model.Categoria"%>
<%@page import="pe.edu.upc.mantenimientoproducto.dao.Dao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Integer id = 0;
    if (request.getParameter("id") != null) {
        id = Integer.parseInt(request.getParameter("id"));
    }
    ProductoDao dao = new ProductoDaoImpl();
    Producto producto = null;
    try {
        producto = (Producto) dao.obtener(id);
    } catch (Exception ex) {
        response.sendRedirect("listar.jsp");
    }
%>

<html>
    <head>
        <%@include file="../template/headerInterno.jsp" %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detalle Producto</title>
    </head>
    <body>
        <%@include file="../template/navbar.jsp" %>

        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1 id="txtTitulo">Detalle de producto</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a href="listar.jsp">Regresar</a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="txtId">ID</label>
                                        <input class="form-control" id="txtIdProducto" type="text" name="txtIdProducto" readonly="" value="<%= (producto == null) ? "" : producto.getIdProducto()%>">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtNombre">Nombre</label>
                                        <input class="form-control" id="txtNombreProducto" type="text" name="txtNombreProducto" readonly="" value="<%= (producto == null) ? "" : producto.getNombre()%>">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtStock">Stock</label>
                                        <input class="form-control" id="txtStock" type="text" name="txtStock" readonly="" value="<%= (producto == null) ? "" : producto.getStock()%>">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtCategoria">Stock</label>
                                        <input class="form-control" id="txtCategoria" type="text" name="txtCategoria" readonly="" value="<%= (producto == null) ? "" : producto.getCategoria().getNombre()%>">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtFecha">Stock</label>
                                        <input class="form-control" id="txtFecha" type="text" name="txtFecha" readonly="" value="<%= (producto == null) ? "" : FechaUtil.formatoDeFecha("yyyy-MM-dd", "dd/MM/yyyy", producto.getFechaVencimiento().toString())%>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-header">
                        <h1>Categoría asociada</h1>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <a href="listar.jsp">Regresar</a>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="txtIdCategoria">ID</label>
                                        <input class="form-control" id="txtIdCategoria" type="text" name="txtIdCategoria" readonly="" value="<%= (producto == null) ? "" : producto.getCategoria().getIdCategoria()%>">
                                    </div>
                                    <div class="form-group">    
                                        <label for="txtNombreCategoria">Stock</label>
                                        <input class="form-control" id="txtNombreCategoria" type="text" name="txtNombreCategoria" readonly="" value="<%= (producto == null) ? "" : producto.getCategoria().getNombre()%>">
                                    </div>
                                    <div class="form-group">    
                                        <label for="txtDescripcion">Descripción</label>
                                        <textarea class="form-control" id="txtDescripcion" name="txtDescripcion" readonly=""><%= (producto == null) ? "" : producto.getCategoria().getDescripcion()%></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../template/footerInterno.jsp" %>
    </body>
</html>
