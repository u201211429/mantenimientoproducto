/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Producto;

/**
 *
 * @author Jamil
 */
public class ProductoDaoImplNGTest {

    public ProductoDaoImplNGTest() {
    }
        
    Integer id;
    Categoria categoriaTest;

    @BeforeClass
    public void beforeClass() {
        try {
            CategoriaDao dao = new CategoriaDaoImpl();
            Categoria categoria = new Categoria();
            categoria.setNombre("TestProducto");
            dao.insertar(categoria);
            categoriaTest = categoria;
            
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    }

    @AfterClass
    public void afterClass() {
        try {
            CategoriaDao dao = new CategoriaDaoImpl();
            dao.eliminar(Categoria.class, categoriaTest.getIdCategoria());
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
    } 

    @BeforeMethod
    public void beforeMethod() {
        id = null;
    }

    @AfterMethod
    public void afterMethod() {
        if (id != null) {
            ProductoDao dao = new ProductoDaoImpl();
            try {
                dao.eliminar(Producto.class, id);
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    /**
     * Test of listar method, of class ProductoDaoImpl.
     */
    @Test
    public void testListar() {
        System.out.println("listar");
        ProductoDao instance = new ProductoDaoImpl();
        Producto producto = new Producto(categoriaTest, "Test", 10, new Date());
        instance.insertar(producto);
        id = producto.getIdProducto();

        List result = null;
        try {
            result = instance.listar();
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of obtener method, of class ProductoDaoImpl.
     */
    @Test
    public void testObtener() {
        System.out.println("obtener");
        ProductoDao instance = new ProductoDaoImpl();
        Producto producto = new Producto();
        producto.setNombre("Test");
        Date date = new Date();
        producto.setFechaVencimiento(date);
        producto.setStock(10);        
        producto.setCategoria(categoriaTest);
        instance.insertar(producto);
        id = producto.getIdProducto();
        
        
        Producto result = null;
        try {
            result = instance.obtener(id);
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
    }
}
