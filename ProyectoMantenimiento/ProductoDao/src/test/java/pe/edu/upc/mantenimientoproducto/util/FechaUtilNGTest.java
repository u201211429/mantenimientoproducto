/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Date;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Jamil
 */
public class FechaUtilNGTest {

    public FechaUtilNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of stringAFecha method, of class FechaUtil.
     */
    @Test
    public void testStringAFecha() {
        System.out.println("stringAFecha");
        String fecha = "10/10/2015";
        String formato = "dd/MM/yyyy";
        String expResult = "Sat Oct 10 00:00:00 COT 2015";
        Date result = FechaUtil.stringAFecha(fecha, formato);
        assertEquals(result.toString(), expResult);
    }

    /**
     * Test of formatoDeFecha method, of class FechaUtil.
     */
    @Test
    public void testFormatoDeFecha() {
        System.out.println("formatoDeFecha");
        String formatoEntrada = "dd-MM-yyyy";
        String formatoSalida = "dd/MM/yyyy";
        String fecha = "10-10-2014";
        String expResult = "10/10/2014";
        String result = FechaUtil.formatoDeFecha(formatoEntrada, formatoSalida, fecha);
        assertEquals(result, expResult);
    }

    /**
     * Test of validarFecha method, of class FechaUtil.
     */
    @Test
    public void testValidarFechaTrue() {
        System.out.println("validarFecha");
        String formatoEntrada = "dd/MM/yyyy";
        String fecha = "10/10/2015";
        boolean expResult = true;
        boolean result = FechaUtil.validarFecha(formatoEntrada, fecha);
        assertEquals(result, expResult);
    }

    @Test
    public void testValidarFechaFalse() {
        System.out.println("validarFecha");
        String formatoEntrada = "dd-MM-yyyy";
        String fecha = "10/10/2015";
        boolean expResult = false;
        boolean result = FechaUtil.validarFecha(formatoEntrada, fecha);
        assertEquals(result, expResult);
    }

    @Test
    public void testStringAFechaError() {
        System.out.println("validarError");
        String formatoEntrada = "dd-MM-yyyy";
        String fecha = "abdcdef12345";
        Date date = FechaUtil.stringAFecha(fecha, formatoEntrada);
        assertNull(date);
    }

    @Test
    public void testFormatoDeFechaError() {
        System.out.println("validarError");
        String formatoEntrada = "dd-MM-yyyy";
        String formatoSalida = "dd/MM/yyyy";
        String fecha = "abdcdef12345";
        String date = FechaUtil.formatoDeFecha(formatoEntrada, formatoSalida, fecha);
        assertNull(date);
    }

    @Test
    public void privateConstructorTest() throws Exception {
        System.out.println("validarConstructor");
        Constructor<FechaUtil> constructors = FechaUtil.class.getDeclaredConstructor();
        constructors.setAccessible(true);
        FechaUtil util = constructors.newInstance();        
        assertNotNull(util);
    }
}
