/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.base.UsuarioDao;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Usuario;

/**
 *
 * @author Jamil
 */
public class CategoriaDaoImplNGTest {

    public CategoriaDaoImplNGTest() {
    }

    Integer id;

    @BeforeMethod
    public void beforeMethod() {
        id = null;
    }

    @AfterMethod
    public void afterMethod() {
        if (id != null) {
            CategoriaDao dao = new CategoriaDaoImpl();
            try {
                dao.eliminar(Categoria.class, id);
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    /**
     * Test of listar method, of class CategoriaDaoImpl.
     */
    @org.testng.annotations.Test
    public void testListar() {
        System.out.println("insertar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria("TestListar");
        categoria.setDescripcion("Test");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();

        System.out.println("listar");
        List result = instance.listar();
        assertNotNull(result);
    }

    /**
     * Test of obtener method, of class CategoriaDaoImpl.
     */
    @org.testng.annotations.Test
    public void testObtener() {
        CategoriaDao instance = new CategoriaDaoImpl();

        Categoria categoria = new Categoria();
        categoria.setNombre("TestObtener");
        categoria.setDescripcion("Test");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();

        Categoria result = null;

        System.out.println("obtener");
        try {
            result = instance.obtener(id);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of buscar method, of class CategoriaDaoImpl.
     */
    @org.testng.annotations.Test
    public void testBuscar() {
        System.out.println("buscar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("TestBuscar");
        categoria.setDescripcion("Test");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();        
        
        System.out.println("buscar");
        Categoria result = null;
        try {
            result = instance.buscar(categoria);
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
    }
    
    @Test(expectedExceptions = NullPointerException.class)
    public void testBuscarError() {
        System.out.println("buscar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = null;
        instance.buscar(categoria);        
    }
}
