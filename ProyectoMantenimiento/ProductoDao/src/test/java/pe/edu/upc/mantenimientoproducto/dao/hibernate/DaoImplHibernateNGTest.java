/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.text.ParseException;
import java.util.Date;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.PropertyValueException;
import org.hibernate.TransientObjectException;
import static org.testng.Assert.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Producto;

/**
 *
 * @author Jamil
 */
public class DaoImplHibernateNGTest {

    public DaoImplHibernateNGTest() {
    }

    Integer id;

    @BeforeMethod
    public void beforeMethod() {
        id = null;
    }

    @AfterMethod
    public void afterMethod() {
        if (id != null) {
            CategoriaDao dao = new CategoriaDaoImpl();
            try {
                dao.eliminar(Categoria.class, id);
            } catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    /**
     * Test of insertar method, of class DaoImplHibernate.
     */
    @Test
    public void testInsertar() {
        System.out.println("insertar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("Test");
        categoria.setDescripcion("Test");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();
        assertNotNull(id);
    }

    /**
     * Test of actualizar method, of class DaoImplHibernate.
     */
    @Test
    public void testActualizar() {
        System.out.println("actualizar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("AntesDeActualizar");
        categoria.setDescripcion("Test");
        try {
            instance.insertar(categoria);
            id = categoria.getIdCategoria();
        } catch (Exception ex) {
            ex.getMessage();
        }

        categoria.setNombre("DespuesDeActualizar");
        instance.actualizar(categoria);

        Categoria categoriaActualizada = instance.obtener(categoria.getIdCategoria());

        assertNotNull(categoriaActualizada);
    }

    /**
     * Test of eliminar method, of class DaoImplHibernate.
     */
    @Test
    public void testEliminar1() {
        System.out.println("eliminar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("TestEliminar");
        categoria.setDescripcion("TestEliminar");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();
        instance.eliminar(Categoria.class, id);

        Categoria categoriaEliminada = null;

        try {
            categoriaEliminada = instance.obtener(id);
        } catch (Exception ex) {

        }
        assertNull(categoriaEliminada);
    }
    
    @Test
    public void testEliminar2() {
        System.out.println("eliminar");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("TestEliminar2");
        categoria.setDescripcion("TestEliminar");
        instance.insertar(categoria);
        id = categoria.getIdCategoria();
        
        System.out.println("listar");
        ProductoDao instanceProducto = new ProductoDaoImpl();
        Producto producto = new Producto(categoria, "Test", 10, new Date());
        instanceProducto.insertar(producto);     
        Integer idProducto = producto.getIdProducto();
        instanceProducto.eliminar(Producto.class, idProducto);

        Producto productoEliminado = null;

        try {
            productoEliminado = instanceProducto.obtener(idProducto);
        } catch (Exception ex) {

        }
        assertNull(productoEliminado);
    }

    @Test(expectedExceptions = PropertyValueException.class)
    public void testErrorInsertar() {
        System.out.println("insertarError");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        instance.insertar(categoria);
    }

    @Test(expectedExceptions = TransientObjectException.class)
    public void testErrorActualizar() {
        System.out.println("actualizarError");
        CategoriaDao instance = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        instance.actualizar(categoria);
    }
}
