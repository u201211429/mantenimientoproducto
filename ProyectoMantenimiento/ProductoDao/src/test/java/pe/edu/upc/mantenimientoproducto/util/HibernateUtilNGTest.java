/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.util;

import java.lang.reflect.Constructor;
import org.hibernate.SessionFactory;
import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Jamil
 */
public class HibernateUtilNGTest {
    
    public HibernateUtilNGTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    /**
     * Test of getSessionFactory method, of class HibernateUtil.
     */
    @Test
    public void testGetSessionFactory() {
        System.out.println("getSessionFactory");
        SessionFactory result = HibernateUtil.getSessionFactory();
        assertNotNull(result);
    }
    
    @Test
    public void privateConstructorTest() throws Exception {
        System.out.println("validarConstructor");
        Constructor<HibernateUtil> constructors = HibernateUtil.class.getDeclaredConstructor();
        constructors.setAccessible(true);
        HibernateUtil util = constructors.newInstance();        
        assertNotNull(util);
    }
    
}
