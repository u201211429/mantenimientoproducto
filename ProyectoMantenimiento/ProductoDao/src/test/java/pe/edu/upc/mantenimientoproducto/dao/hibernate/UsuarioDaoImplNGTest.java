/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.text.ParseException;
import java.util.List;
import static org.testng.Assert.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.UsuarioDao;
import pe.edu.upc.mantenimientoproducto.model.Usuario;

/**
 *
 * @author Jamil
 */
public class UsuarioDaoImplNGTest {
    
    public UsuarioDaoImplNGTest() {
    }

    Integer id;
    
    @BeforeMethod
    public void beforeMethod() {
        id = null;        
    }
    
    @AfterMethod
    public void afterMethod() {
        if (id != null) {
            UsuarioDao dao = new UsuarioDaoImpl();
            try {
                dao.eliminar(Usuario.class, id);
            }
            catch (Exception ex) {
                ex.getMessage();
            }
        }
    }

    /**
     * Test of listar method, of class UsuarioDaoImpl.
     */
    @Test
    public void testListar() {
        System.out.println("listar");
        UsuarioDao instance = new UsuarioDaoImpl();
        Usuario usuario = new Usuario();
        usuario.setNombreUsuario("Test");
        usuario.setClave("Test");
        instance.insertar(usuario);
        id = usuario.getIdUsuario();
        
        List result = null;
        
        try {
            result = instance.listar();
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
    }

    /**
     * Test of obtener method, of class UsuarioDaoImpl.
     */
    @Test
    public void testObtener() {
        System.out.println("obtener");
        UsuarioDao instance = new UsuarioDaoImpl();
        Usuario usuario = new Usuario("Test", "Test");
        instance.insertar(usuario);
        id = usuario.getIdUsuario();
        
        Usuario result = null;
        try {
            result = instance.obtener(id);
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(result);
        
    }

    /**
     * Test of buscar method, of class UsuarioDaoImpl.
     */
    @Test
    public void testBuscar() {
        System.out.println("buscar");
        UsuarioDao instance = new UsuarioDaoImpl();
        Usuario usuario = new Usuario();
        usuario.setNombreUsuario("Test");
        usuario.setClave("Test");
        instance.insertar(usuario);
        id = usuario.getIdUsuario();        
        
        Usuario result = null;
        try {
            result = instance.buscar(usuario);
        }
        catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertNotNull(usuario);
    }
    
    @Test(expectedExceptions = NullPointerException.class)
    public void testBuscarError() {
        System.out.println("buscar");
        UsuarioDao instance = new UsuarioDaoImpl();
        Usuario usuario = null;
        instance.buscar(usuario);        
    }
}
