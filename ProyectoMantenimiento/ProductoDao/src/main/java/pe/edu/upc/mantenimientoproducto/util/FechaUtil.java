/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Jamil
 */
public class FechaUtil {

    private FechaUtil() {
    }

    public static Date stringAFecha(String fecha, String formato){
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        Date date;
        try {
            date = sdf.parse(fecha);
        } catch (ParseException ex) {
            date = null;
        }
        return date;
    }

    public static String formatoDeFecha(String formatoEntrada, String formatoSalida, String fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatoEntrada);
        Date date;
        String salida;

        try {
            date = sdf.parse(fecha);
            sdf = new SimpleDateFormat(formatoSalida);
            salida = sdf.format(date);
        } catch (ParseException ex) {
            salida = null;
        }       

        return salida;
    }

    public static boolean validarFecha(String formatoEntrada, String fecha) {
        boolean valido = true;
        SimpleDateFormat sdf = new SimpleDateFormat(formatoEntrada);
        sdf.setLenient(false);

        try {
            sdf.parse(fecha);
        } catch (ParseException ex) {
            valido = false;
        }
        return valido;
    }
}
