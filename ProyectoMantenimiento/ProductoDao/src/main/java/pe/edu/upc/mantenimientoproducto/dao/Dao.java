/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao;

import java.util.List;

/**
 *
 * @author Jamil
 */
public interface Dao<E> {

    public List<E> listar();

    public E obtener(int id);

    public E insertar(E t);

    public void actualizar(E t);

    public void eliminar(final Class<E> type, int id);
}
