/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pe.edu.upc.mantenimientoproducto.util.HibernateUtil;

public class DaoImplHibernate<T> {

    Transaction transaction;
    Session session;
    
    

    public T insertar(T t) {
        try {
            persistir(t, false);
        } catch (Exception ex) {
            throw ex;
        }
        return t;
    }

    public void actualizar(T t) {
        try {
            persistir(t, true);
        } catch (Exception ex) {
            throw ex;
        }
    }

    public void eliminar(final Class<T> type, int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            transaction = session.beginTransaction();
            T p = (T) session.load(type, new Integer(id));
            session.delete(p);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
    }

    private T persistir(T t, boolean actualizar) {
        session = HibernateUtil.getSessionFactory().openSession();
        try {
            transaction = session.beginTransaction();
            if (actualizar) {
                session.update(t);
            } else {
                session.persist(t);
            }
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return t;
    }
}
