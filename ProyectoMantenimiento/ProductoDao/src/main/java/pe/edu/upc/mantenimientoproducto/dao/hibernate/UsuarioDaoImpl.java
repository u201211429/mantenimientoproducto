/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pe.edu.upc.mantenimientoproducto.model.Usuario;
import pe.edu.upc.mantenimientoproducto.util.HibernateUtil;
import pe.edu.upc.mantenimientoproducto.dao.base.UsuarioDao;

/**
 *
 * @author Jamil
 */
public class UsuarioDaoImpl extends DaoImplHibernate<Usuario> implements UsuarioDao {

    Transaction transaction;
    Session session;

    @Override
    public List<Usuario> listar() {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        transaction = session.beginTransaction();
        List<Usuario> usuarios = session.createQuery("from Usuario").list();
        transaction.commit();

        return usuarios;

    }

    @Override
    public Usuario obtener(int id) {
        session = HibernateUtil.getSessionFactory().getCurrentSession();
        transaction = session.beginTransaction();
        Usuario p = (Usuario) session.load(Usuario.class, new Integer(id));
        transaction.commit();
        return p;
    }

    @Override
    public Usuario buscar(Usuario t) {
        session = HibernateUtil.getSessionFactory().openSession();
        Usuario usuario = null;
        try {
            transaction = session.beginTransaction();
            Query q = session.createQuery("FROM Usuario E WHERE E.clave = :clave AND E.nombreUsuario = :nombre ")
                    .setParameter("clave", t.getClave())
                    .setParameter("nombre", t.getNombreUsuario());
            transaction.commit();
            usuario = (Usuario) q.uniqueResult();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return usuario;
    }
}
