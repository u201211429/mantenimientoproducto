/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.base;

import pe.edu.upc.mantenimientoproducto.dao.Dao;
import pe.edu.upc.mantenimientoproducto.model.Usuario;

/**
 *
 * @author Jamil
 */
public interface UsuarioDao extends Dao<Usuario> {
    public Usuario buscar(Usuario t);        
}
