/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.util.HibernateUtil;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;

/**
 *
 * @author Jamil
 */
public class CategoriaDaoImpl extends DaoImplHibernate<Categoria> implements CategoriaDao {

    @Override
    public List<Categoria> listar() {
        session = HibernateUtil.getSessionFactory().openSession();
        List<Categoria> categorias;
        try {
            transaction = session.beginTransaction();
            categorias = session.createQuery("from Categoria").list();
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }

        return categorias;
    }

    @Override
    public Categoria obtener(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        Categoria c;
        try {
            transaction = session.beginTransaction();
            c = (Categoria) session.load(Categoria.class, new Integer(id));
            Hibernate.initialize(c);
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return c;

    }

    @Override
    public Categoria buscar(Categoria t) {
        session = HibernateUtil.getSessionFactory().openSession();
        Categoria c;
        try {
            transaction = session.beginTransaction();
            Query q = session.createQuery("FROM Categoria E WHERE E.nombre = :nombre ")
                    .setParameter("nombre", t.getNombre());
            transaction.commit();
            c = (Categoria) q.uniqueResult();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return c;
    }
}
