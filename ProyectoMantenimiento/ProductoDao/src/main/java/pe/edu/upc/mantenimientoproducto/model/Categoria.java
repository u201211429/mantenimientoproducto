package pe.edu.upc.mantenimientoproducto.model;
// Generated May 10, 2015 11:25:42 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Categoria generated by hbm2java
 */
public class Categoria implements java.io.Serializable {

    private Integer idCategoria;
    private String nombre;
    private String descripcion;
    private transient Set productos = new HashSet(0);

    public Categoria() {
    }

    public Categoria(String nombre) {
        this.nombre = nombre;
        this.descripcion = "";
    }

    public Integer getIdCategoria() {
        return this.idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Set getProductos() {
        return this.productos;
    }

    public void setProductos(Set productos) {
        this.productos = productos;
    }

}
