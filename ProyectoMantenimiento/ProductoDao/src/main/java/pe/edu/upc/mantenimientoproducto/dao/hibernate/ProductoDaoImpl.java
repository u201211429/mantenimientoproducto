/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.mantenimientoproducto.dao.hibernate;

import java.util.List;
import org.hibernate.Hibernate;
import pe.edu.upc.mantenimientoproducto.model.Producto;
import pe.edu.upc.mantenimientoproducto.util.HibernateUtil;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;

/**
 *
 * @author Jamil
 */
public class ProductoDaoImpl extends DaoImplHibernate<Producto> implements ProductoDao {

    @Override
    public List<Producto> listar() {
        session = HibernateUtil.getSessionFactory().openSession();
        List<Producto> productos = null;
        try {
            transaction = session.beginTransaction();
            productos = session.createQuery("from Producto").list();
            for (Producto producto : productos) {
                Hibernate.initialize(producto.getCategoria());
            }
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }

        return productos;

    }

    @Override
    public Producto obtener(int id) {
        session = HibernateUtil.getSessionFactory().openSession();
        Producto p;
        try {
            transaction = session.beginTransaction();
            p = (Producto) session.load(Producto.class, new Integer(id));
            Hibernate.initialize(p);
            Hibernate.initialize(p.getCategoria());
            transaction.commit();
        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            session.close();
        }
        return p;
    }
}