/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Jamil
 */
public class ListadoCategoriasNGTest {

    private Selenium driver;

    public ListadoCategoriasNGTest() {
    }

    @Test
    public void listarProductosOK() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");

        Assert.assertEquals(driver.getText("txtTitulo"), "Ingresar o editar categorías");
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();

    }

    @AfterMethod
    public void afterMethod() throws Exception {
        driver.stop();
    }
}
