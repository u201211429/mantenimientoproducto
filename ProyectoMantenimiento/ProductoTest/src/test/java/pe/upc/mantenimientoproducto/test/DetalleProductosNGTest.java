/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Producto;
import pe.edu.upc.mantenimientoproducto.util.FechaUtil;

/**
 *
 * @author Jamil
 */
public class DetalleProductosNGTest {

    public DetalleProductosNGTest() {
    }

    private Selenium driver;
    private String idProducto;
    private String idCategoria;

    @Test
    public void DetalleProductoOK() {
 
            driver.open("/ProductoWeb/menu.jsp");
            driver.waitForPageToLoad("30000");
            driver.click("btnProductos");
            driver.waitForPageToLoad("30000");
            driver.click("btnDetalle" + idProducto);
            driver.waitForPageToLoad("30000");

            Assert.assertEquals(driver.getText("txtTitulo"), "Detalle de producto");
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();
        
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("PruebaDetalleProducto");
        categoria.setDescripcion("");
        daoCategoria.insertar(categoria);
        idCategoria = categoria.getIdCategoria().toString();

        ProductoDao daoProducto = new ProductoDaoImpl();
        Producto producto = new Producto();
        producto.setNombre("Inka Inglesa");
        producto.setStock(2000);
        producto.setCategoria(categoria);
        producto.setFechaVencimiento(FechaUtil.stringAFecha("01/05/2015", "dd/MM/yyyy"));
        daoProducto.insertar(producto);
        idProducto = producto.getIdProducto().toString();
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        driver.stop();
        ProductoDao daoProducto = new ProductoDaoImpl();
        daoProducto.eliminar(Producto.class, Integer.parseInt(idProducto));
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        daoCategoria.eliminar(Categoria.class, Integer.parseInt(idCategoria));
    }
}
