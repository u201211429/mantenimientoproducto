/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;

/**
 *
 * @author Jamil
 */
public class DetalleCategoriasNGTest {

    public DetalleCategoriasNGTest() {
    }

    private Selenium driver;
    private String idCategoria;

    @Test
    public void DetalleCategoriaOK() {

        driver.open("/ProductoWeb/menu.jsp");
        driver.waitForPageToLoad("30000");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.click("btnDetalle" + idCategoria);
        driver.waitForPageToLoad("30000");

        Assert.assertEquals(driver.getText("txtTitulo"), "Detalle de categoría");
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();

        CategoriaDao dao = new CategoriaDaoImpl();
        Categoria categoria = new Categoria();
        categoria.setNombre("PruebaDetalle");
        categoria.setDescripcion("");
        
        idCategoria = dao.insertar(categoria).getIdCategoria().toString();
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        driver.stop();
        CategoriaDao dao = new CategoriaDaoImpl();
        dao.eliminar(Categoria.class, Integer.parseInt(idCategoria));

    }
}
