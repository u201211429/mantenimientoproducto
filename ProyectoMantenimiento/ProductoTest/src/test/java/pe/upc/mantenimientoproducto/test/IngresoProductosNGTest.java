/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.base.ProductoDao;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.ProductoDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;
import pe.edu.upc.mantenimientoproducto.model.Producto;

/**
 *
 * @author Jamil
 */
public class IngresoProductosNGTest {

    public IngresoProductosNGTest() {
    }

    private Selenium driver;
    String idCategoria;
    String idProducto;

    @Test
    public void ingresarProductoOK() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "2000");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Pattern pattern = Pattern.compile("id=(\\d+)");
        Matcher matcher = pattern.matcher(driver.getLocation());

        if (matcher.find()) {
            idProducto = matcher.group(1);
        }

        Assert.assertEquals(driver.getText("txtMensaje"), "El producto ha sido insertado correctamente.");
    }

    @Test
    public void errorFechaInvalida() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "2015/1/1");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "La fecha debe estar en formato dd/mm/yyyy.");
    }

    @Test
    public void errorFechaNula() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("La fecha de vencimiento es obligatoria.")));
    }

    @Test
    public void errorStockFueraRango() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "-1");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("El stock debe ser valido.")));
    }

    @Test
    public void errorStockNulo() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("El stock es un campo obligatorio.")));
    }

    @Test(enabled = false)
    public void errorCategoriaNula() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.runScript("document.getElementById('txtCategoria').value = '999999'");
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría es un campo obligatorio.")));
    }

    @Test(enabled = false)
    public void errorCategoriaNoExiste() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", "19999");
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría debe estar registrada en el sistema.")));
    }

    @Test
    public void errorNombreNulo() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("El nombre es un campo obligatorio.")));
    }

    @Test
    public void errorNombreFueraRango() {
        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.runScript("document.getElementById('txtNombreProducto').value = 'Inka cola inglesa pepsi fanta concordia'");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("El nombre es más largo que lo permitido.")));
    }

    @Test
    public void actualizarProductoOK() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "2000");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "El producto se modificó correctamente.");
    }

    @Test
    public void errorFechaInvalidaActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "2000");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "2015/1/1");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "La fecha debe estar en formato dd/mm/yyyy.");
    }

    @Test
    public void errorFechaNulaActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "2000");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue(driver.getText("txtMensaje").contains("La fecha de vencimiento es obligatoria."));
    }

    @Test
    public void errorStockNuloActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue(driver.getText("txtMensaje").contains("El stock es un campo obligatorio."));
    }

    @Test
    public void errorNombreNuloActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue(driver.getText("txtMensaje").contains("El nombre es un campo obligatorio."));
    }

    @Test
    public void errorNombreFueraRangoActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.runScript("document.getElementById('txtNombreProducto').setAttribute('value', 'Inka cola inglesa pepsi fanta concordia')");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue(driver.getText("txtMensaje").contains("El nombre es más largo que lo permitido."));
    }

    @Test(enabled = false)
    public void errorCategoriaNoExisteActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", "19999");
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría debe estar registrada en el sistema.")));
    }

    @Test(enabled = false)
    public void errorCategoriaNulaActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "7");
        driver.type("txtCategoria", " ");
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue((driver.getText("txtMensaje").contains("La categoría es un campo obligatorio.")));
    }

    @Test
    public void errorStockFueraDeRangoActualizar() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + idProducto);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombreProducto", "Inka Inglesa");
        driver.type("txtStock", "-1");
        driver.type("txtCategoria", idCategoria);
        driver.type("txtFecha", "01/05/2015");
        driver.submit("frmProducto");

        driver.waitForPageToLoad("30000");
        Assert.assertTrue(driver.getText("txtMensaje").contains("El stock debe ser valido."));
    }

    @Test
    public void eliminarProductoOK() {
        ProductoDao dao = new ProductoDaoImpl();
        CategoriaDao daoCategoria = new CategoriaDaoImpl();
        Categoria categoria = daoCategoria.obtener(Integer.parseInt(idCategoria));
        idProducto = dao.insertar(new Producto(categoria, "Inka Inglesa", 7, new Date()))
                .getIdProducto()
                .toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnProductos");
        driver.waitForPageToLoad("30000");
        driver.click("btnEliminar" + idProducto);

        driver.waitForPageToLoad("30000");
        idProducto = null;
        Assert.assertTrue(driver.getText("txtMensaje").contains("Se eliminó el producto satisfactoriamente."));

    }

    @BeforeMethod
    public void beforeMethod() {
        CategoriaDao dao = new CategoriaDaoImpl();
        idCategoria = dao.insertar(new Categoria("Gaseosas")).getIdCategoria().toString();
    }

    @AfterMethod
    public void afterMethod() {
        if (idProducto != null) {
            ProductoDao dao = new ProductoDaoImpl();
            dao.eliminar(Producto.class, Integer.parseInt(idProducto));
            idProducto = null;
        }
        if (idCategoria != null) {
            CategoriaDao dao = new CategoriaDaoImpl();
            dao.eliminar(Categoria.class, Integer.parseInt(idCategoria));
            idCategoria = null;
        }
    }

    @BeforeClass
    public void beforeClass() {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();
    }

    @AfterClass
    public void afterClass() {
        driver.stop();
    }
}
