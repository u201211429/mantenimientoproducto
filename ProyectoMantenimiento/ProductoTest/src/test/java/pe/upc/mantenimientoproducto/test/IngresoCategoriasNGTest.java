/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pe.edu.upc.mantenimientoproducto.dao.base.CategoriaDao;
import pe.edu.upc.mantenimientoproducto.dao.hibernate.CategoriaDaoImpl;
import pe.edu.upc.mantenimientoproducto.model.Categoria;

/**
 *
 * @author Jamil
 */
public class IngresoCategoriasNGTest {

    private Selenium driver;
    String id = null;
    String id2 = null;

    @Test
    public void ingresarCategoriaOK() {
        try {
            driver.open("/ProductoWeb/menu.jsp");
            driver.waitForPageToLoad("30000");
            driver.click("btnCategorias");
            driver.waitForPageToLoad("30000");
            driver.type("txtNombre", "Lácteos");
            driver.type("txtDescripcion", "Productos provenientes de leche");
            driver.submit("frmCategoria");

            driver.waitForPageToLoad("30000");

            Pattern pattern = Pattern.compile("id=(\\d+)");
            Matcher matcher = pattern.matcher(driver.getLocation());

            if (matcher.find()) {
                id = matcher.group(1);
            }

            Assert.assertEquals(driver.getText("txtMensaje"), "La categoría ha sido insertada correctamente.");

        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void errorNombreNuloInsertar() {
        try {
            driver.open("/ProductoWeb/menu.jsp");
            driver.waitForPageToLoad("30000");
            driver.click("btnCategorias");
            driver.waitForPageToLoad("30000");
            driver.type("txtDescripcion", "Productos provenientes de leche");
            driver.submit("frmCategoria");
            driver.waitForPageToLoad("30000");
            Assert.assertEquals(driver.getText("txtMensaje"), "El nombre es un campo obligatorio.");
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void errorNombreRangoInsertar() {
        try {
            driver.open("/ProductoWeb/menu.jsp");
            driver.waitForPageToLoad("30000");
            driver.click("btnCategorias");
            driver.waitForPageToLoad("30000");
            driver.type("txtNombre", "Lácteos y quesos importados variados");
            driver.type("txtDescripcion", "Productos provenientes de leche");
            driver.submit("frmCategoria");

            driver.waitForPageToLoad("30000");
            Assert.assertEquals(driver.getText("txtMensaje"), "El nombre es más largo que lo permitido.");
        } catch (Exception ex) {
            Assert.fail(ex.getMessage());
        }
    }

    @Test
    public void errorNombreYaExisteInsertar() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.type("txtNombre", "Lácteos");
        driver.type("txtDescripcion", "Productos provenientes de leche");
        driver.submit("frmCategoria");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "La categoría Lácteos ya existe.");
    }

    //Aca van las de actualizacion
    @Test
    public void actualizarCategoriaOK() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + id);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombre", "Licores");
        driver.type("txtDescripcion", "Bebidas alcohólicas +18");
        driver.submit("frmCategoria");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "La categoría se modificó satisfactoriamente.");
    }

    //Modificar categoria error
    @Test
    public void errorNombreNuloActualizar() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + id);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombre", "");
        driver.type("txtDescripcion", "Productos provenientes de la leche");
        driver.submit("frmCategoria");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "El nombre es un campo obligatorio.");
    }

    @Test
    public void errorNombreRangoActualizar() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.click("btnEditar" + id);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombre", "Lácteos y quesos importados variados");
        driver.type("txtDescripcion", "Productos provenientes de la leche");
        driver.submit("frmCategoria");

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "El nombre es más largo que lo permitido.");
    }

    @Test
    public void eliminarCategoriaOK() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");
        driver.click("btnEliminar" + id);

        driver.waitForPageToLoad("30000");
        Assert.assertEquals(driver.getText("txtMensaje"), "Se eliminó la categoría satisfactoriamente.");
        id = null;
    }

    @Test
    public void errorNombreYaExisteActualizar() {
        CategoriaDao dao = new CategoriaDaoImpl();
        id = dao.insertar(new Categoria("Gaseosas")).getIdCategoria().toString();
        id2 = dao.insertar(new Categoria("Lácteos")).getIdCategoria().toString();

        driver.open("/ProductoWeb/menu.jsp");
        driver.click("btnCategorias");
        driver.waitForPageToLoad("30000");

        driver.click("btnEditar" + id2);
        driver.waitForPageToLoad("30000");
        driver.type("txtNombre", "Gaseosas");
        driver.type("txtDescripcion", "Productos provenientes de la leche");
        driver.submit("frmCategoria");
        driver.waitForPageToLoad("30000");

        Assert.assertEquals(driver.getText("txtMensaje"), "La categoría Gaseosas ya existe.");
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
    }

    @AfterMethod
    public void afterMethod() throws Exception {
        if (id != null) {
            CategoriaDao dao = new CategoriaDaoImpl();
            dao.eliminar(Categoria.class, Integer.parseInt(id));
            id = null;
        }
        if (id2 != null) {
            CategoriaDao dao = new CategoriaDaoImpl();
            dao.eliminar(Categoria.class, Integer.parseInt(id2));
            id2 = null;
        }
    }

    @BeforeClass
    public void beforeClass() {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();
    }

    @AfterClass
    public void afterClass() {
        driver.stop();
    }
}
