/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.upc.mantenimientoproducto.test;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author Jamil
 */
public class LoginNGTest {

    private Selenium driver;

    public LoginNGTest() {
    }

    @Test(enabled = true)
    public void LoginOK() {
        driver.open("/ProductoWeb/");
        driver.type("txtUsuario", "admin");
        driver.type("txtClave", "adminadmin");
        driver.click("btnIngresar");
        driver.waitForPageToLoad("30000");

        //Assert.assertTrue("Datos Correctos").equalsIgnoreCase(driver.getText("txtMensaje")));            
        Assert.assertEquals(driver.getText("txtMensaje"), "Datos correctos");
    }

    @Test(enabled = true)
    public void errorUsuarioEmpleadoNulo() {
        driver.open("/ProductoWeb/");
        driver.type("txtClave", "adminadmin");
        driver.submit("frmLogin");

        driver.waitForPageToLoad("30000");

        //Assert.assertTrue("Datos Correctos").equalsIgnoreCase(driver.getText("txtMensaje")));            
        Assert.assertEquals(driver.getText("txtMensaje"), "Se debe ingresar el nombre de usuario.");
    }

    @Test(enabled = true)
    public void errorClaveEmpleadoNulo() {
        driver.open("/ProductoWeb/");
        driver.type("txtUsuario", "adminadmin");
        driver.submit("frmLogin");

        driver.waitForPageToLoad("30000");

        Assert.assertTrue((driver.getText("txtMensaje").contains("Se debe ingresar la clave.")));
    }

    @Test(enabled = true)
    public void errorUsuarioYclaveIncorrectos() {
        driver.open("/ProductoWeb/");
        driver.type("txtUsuario", "user01");
        driver.type("txtClave", "12345678");
        driver.submit("frmLogin");

        driver.waitForPageToLoad("30000");
           
        Assert.assertEquals(driver.getText("txtMensaje"), "Datos incorrectos");
    }

    @BeforeClass
    public void beforeMethod() throws Exception {
        driver = new DefaultSelenium("localhost", 4444, "*firefox C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe", "http://localhost:8080/");
        driver.start();

    }

    @AfterClass
    public void afterMethod() throws Exception {
        driver.stop();
    }
}
